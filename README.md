# simple-chat

## Архитектура

```mermaid
graph LR
    subgraph Heroku[Heroku]
        subgraph Docker-H[Docker]
            Flask(Python Flask API)
        end
    end
    subgraph DO[Digital Ocean Droplet]
        subgraph Docker-D[Docker]
            DB(MongoDB)
        end
    end
    subgraph Android[Android]
        subgraph JVM[JVM]
            SC(Simple Chat Client)
        end
    end

    SC-.->Flask-.->DB
    DB-.->Flask-.->SC
```

## Регистрация пользователя

```mermaid
sequenceDiagram
    participant client
    participant server
    participant db
    Note over client: Registration
    client->>+server: POST /register {"username": str, "password": str}
    server->>+db: chat.users.find()
    alt user does not exist
        rect rgba(195, 216, 44, .5)
            db-->>server: NOT FOUND
            server->>db: chat.users.insert()
            db-->>server: INSERTED
            server-->>client: 201 {"message": str}
        end
    else user exists
        rect rgba(255, 81, 81, .5)
            db-->>-server: FOUND
            server-->>-client: 400 {"error_msg": str}
        end
    end
```

## Аутентификация

```mermaid
sequenceDiagram
    participant client
    participant server
    participant db
    Note over client: Authentication
    client->>+server: GET /login {"username": str, "password": str}
    server->>+db: chat.users.find()
    alt user and pass exist
        rect rgba(195, 216, 44, .5)
            db-->>server: FOUND
            server-->>client: 200 {"token": str, "users": list}
        end
    else wrong user/pass
        rect rgba(255, 81, 81, .5)
            db-->>-server: NOT FOUND
            server-->>-client: 400 {"error_msg": str}
        end
    end
```

## Вход в чат

```mermaid
sequenceDiagram
    participant client
    participant server
    participant db
    Note over client: Entering room
    client->>+server: GET /room {"token": str, "user_to": str}
    server->>+db: chat.rooms.find()
    opt Room does not exist
        rect rgba(255, 193, 53, .5)
            db-->>-server: NOT FOUND
            server->>+db: db.rooms.insert()
        end
    end
    db-->>-server: FOUND
    server-->>-client: 200 {"room_id": str, "messages": list}
```

## Отправка сообщения

```mermaid
sequenceDiagram
    participant client
    participant server
    participant db
    Note over client: Sending message
    client->>+server: POST /room/send {"token": str, "room_id": str, "message": str}
        server->>+db: chat.messages.insert()
        db-->>-server: _id

        server->>+db: chat.rooms.update()
        db-->>-server: OK
    server-->>-client: 201 {"message": str}
```

## Получение новых сообщений

```mermaid
sequenceDiagram
    participant client
    participant server
    participant db
    Note over client: Fetching messages
    client->>+server: POST /room/send {"token": str, "room_id": str, "since": str}
        server->>+db: chat.rooms.find()
        db-->>-server: FOUND
    server-->>-client: 200 {"room_id": str, "messages": list}
```
