"""Testing chat API."""
import requests
import pytest
from hashlib import md5, sha1

from sibutils.io import read_config
from sibutils.db import get_db
from sibutils.logging import get_colored_logger


US_1 = "test"
PA_1 = "test"
US_2 = "te-1"
PA_2 = "te-1"
MSG = "test message"

CONFIG = read_config("config.json")["tests"]
DB_CONFIG = CONFIG["db"]
LOG = get_colored_logger("API-TESTS", CONFIG["log_level"])


def _cleanup_user(username):
    db = get_db(DB_CONFIG)
    db.users.delete_one({"username": username})


def _cleanup_room(room_id):
    db = get_db(DB_CONFIG)
    LOG.debug("Erasing room: %s", room_id)
    db.rooms.delete_one({"room_id": room_id})
    db.messages.delete_many({"room_id": room_id})
    return


def _register(username, password):
    request = {
        "username": username,
        "password": password
    }
    raw = requests.post("http://127.0.0.1:1313/register", json=request)
    return raw


def _login(username, password):
    request = {
        "username": username,
        "password": password
    }
    raw = requests.post("http://127.0.0.1:1313/login", json=request)
    return raw


def _room(token, user_to):
    request = {
        "token": token,
        "user_to": user_to
    }
    raw = requests.get("http://127.0.0.1:1313/room", json=request)
    return raw


def _room_send(token, room_id, message):
    request = {
        "token": token,
        "room_id": room_id,
        "message": message
    }
    raw = requests.post("http://127.0.0.1:1313/room/send", json=request)
    return raw


@pytest.fixture
def cleanup():
    LOG.warning("Cleaning up")
    _cleanup_user(US_1)
    _cleanup_user(US_2)

    hash_1 = int(sha1(US_1.lower().encode()).hexdigest(), 16) % (10 ** 8)
    hash_2 = int(sha1(US_2.lower().encode()).hexdigest(), 16) % (10 ** 8)
    md5hash = md5()
    md5hash.update(str(hash_1 + hash_2).encode())
    room_id = md5hash.hexdigest()
    _cleanup_room(room_id)


@pytest.fixture
def register(cleanup):
    # pylint: disable=redefined-outer-name
    raw = _register(US_1, PA_1)
    return raw


@pytest.fixture
def login(register):
    # pylint: disable=redefined-outer-name
    raw = _login(US_1, PA_1)
    return raw


@pytest.fixture
def room(login):
    # pylint: disable=redefined-outer-name
    _register(US_2, PA_2)
    log_raw = _login(US_1, PA_1)
    token = log_raw.json()["token"]
    raw = _room(token, US_2)
    return raw


@pytest.fixture
def room_send(room):
    # pylint: disable=redefined-outer-name
    _register(US_2, PA_2)
    _login(US_2, PA_2)
    log_raw = _login(US_1, PA_1)
    token = log_raw.json()["token"]
    raw_room = _room(token, US_2)
    raw = _room_send(
        token=token,
        room_id=raw_room.json()["room_id"],
        message=MSG
    )
    return raw


def check_message():
    # pylint: disable=redefined-outer-name
    log_raw = _login(US_2, PA_2)
    token = log_raw.json()["token"]
    raw = _room(token, US_1)
    return raw


def test_register_route(register):
    # pylint: disable=redefined-outer-name
    raw = register
    data = raw.json()
    LOG.info("/register test: %s", raw.json())
    assert raw.status_code == 201
    assert "message" in data
    assert "token" not in data
    assert "password" not in data


def test_login_route(login):
    # pylint: disable=redefined-outer-name
    raw = login
    data = raw.json()
    LOG.info("/login test: %s", raw.json())
    assert raw.status_code == 200
    assert "token" in data
    assert "users" in data
    assert "password" not in data


def test_room_route(room):
    # pylint: disable=redefined-outer-name
    raw = room
    data = raw.json()
    LOG.info("/room test: %s", raw.json())

    assert "room_id" in data
    assert "created_on" in data
    assert "last_mesg_on" in data
    assert "messages" in data


def test_room_send_route(room_send):
    # pylint: disable=redefined-outer-name
    raw = room_send
    data = raw.json()
    LOG.info("/room/send test: %s", raw.json())

    raw = check_message()
    raw_message = raw.json()["messages"][0]
    message = raw_message["message"]

    LOG.info("/room/send message recieved: %s", raw.json())
    assert "message" in data
    assert message == MSG
