FROM python:3.7

WORKDIR /opt
RUN git clone https://gitlab.com/maxim1317/sibutils.git
WORKDIR /opt/sibutils
RUN pip3 install -r requirements.txt
RUN pip3 install -e .

WORKDIR /home
COPY api/requirements.txt .

RUN pip3 install -r requirements.txt

COPY api api
COPY run.py .
COPY config.json config.json

CMD python3 run.py $PORT

