"""Flask service."""
import os
import datetime as dt
from flask import (
    Flask,
    request,
    jsonify
    # abort
    # Response
)
from flasgger import Swagger
# from werkzeug.exceptions import BadRequest

from sibutils.io import read_config
from sibutils.logging import get_colored_logger

from api.user import (
    User,
    AuthFailed,
    UserExists
)
from api.room import Room

FULL_CONFIG = read_config('config.json')
CONFIG = FULL_CONFIG['api']
DB_CONFIG = FULL_CONFIG['db']

LOG = get_colored_logger('API', CONFIG['log_level'])
get_colored_logger('werkzeug', CONFIG['werkzeug_log_level'])

APP = Flask(__name__)
Swagger(APP)


@APP.route('/login', methods=["GET", "POST"])
def login_route():
    """Authenticate user."""
    credentials = request.get_json()
    LOG.debug("New /login request: %s", credentials)
    try:
        user = User(
            config=FULL_CONFIG,
            username=credentials["username"],
            password=credentials["password"]
        )
        response = user.auth()
        code = 200
    except (AuthFailed, ValueError) as error:
        response = {
            "error_msg": str(error)
        }
        code = 400

    LOG.debug("/login answer: %s", response)
    return jsonify(response), code


@APP.route('/register', methods=["POST"])
def register_route():
    """Register new user."""
    credentials = request.get_json()
    LOG.debug("New /register request: %s", credentials)
    try:
        user = User(
            config=FULL_CONFIG,
            username=credentials["username"],
            password=credentials["password"]
        )
        response = user.register()
        code = 201
    except (UserExists, ValueError) as error:
        response = {
            "error_msg": str(error)
        }
        code = 400

    LOG.debug("/register answer: %s", response)
    return jsonify(response), code


@APP.route('/room', methods=["GET", "POST"])
def room_route():
    """Enter the room."""
    credentials = request.get_json()
    LOG.debug("New /room request: %s", credentials)
    try:
        user = User(
            config=FULL_CONFIG,
            token=credentials["token"],
        )
        user.auth()
    except AuthFailed as error:
        response = {
            "error_msg": str(error)
        }
        code = 400
        return jsonify(response), code

    room = Room(
        config=FULL_CONFIG,
        user_from=user,
        user_to=credentials["user_to"]
    )
    response = room.fetch_all()
    code = 200

    LOG.debug("/room answer: %s", response)
    return jsonify(response), code


@APP.route('/room/send', methods=["POST"])
def room_send_route():
    """Enter the room."""
    credentials = request.get_json()
    LOG.debug("New /room/send request: %s", credentials)
    try:
        user = User(
            config=FULL_CONFIG,
            token=credentials["token"],
        )
        user.auth()
    except AuthFailed as error:
        response = {
            "error_msg": str(error)
        }
        code = 400
        return jsonify(response), code

    room = Room(
        config=FULL_CONFIG,
        user_from=user,
        room_id=credentials["room_id"]
    )
    response = room.send_message(credentials["message"])
    code = 201

    LOG.debug("/room/send answer: %s", response)
    return jsonify(response), code


@APP.route('/room/fetch', methods=["GET", "POST"])
def room_fetch_route():
    """Fetch messages."""
    credentials = request.get_json()
    LOG.debug("New /room/fetch request: %s", credentials)
    try:
        user = User(
            config=FULL_CONFIG,
            token=credentials["token"],
        )
        user.auth()
    except AuthFailed as error:
        response = {
            "error_msg": str(error)
        }
        code = 400
        return jsonify(response), code

    room = Room(
        config=FULL_CONFIG,
        user_from=user,
        room_id=credentials["room_id"]
    )
    response = room.fetch_since(
        dt.datetime.utcfromtimestamp(2 + float(credentials["since"]) / 1000)
    )
    code = 200

    LOG.debug("/room/fetch answer: %s", response)
    return jsonify(response), code


def run():
    """Run Flask server."""

    try:
        port = int(os.environ.get('PORT', CONFIG["port"]))
    except Exception:
        port = CONFIG['port']
    APP.run(
        host=CONFIG['host'],
        port=port,
        threaded=CONFIG['threaded'],
        debug=CONFIG['debug_mode']
    )


if __name__ == '__main__':
    run()
