"""Class User for working with users."""
import datetime as dt
from hashlib import md5
from secrets import token_urlsafe
from pymongo import ReturnDocument

from sibutils.db import get_db
from sibutils.logging import get_colored_logger


class AuthFailed(Exception):
    """Raised when user provided wrong credentials."""

    def __init__(self):
        """Create AuthFailed exception."""
        message = "Wrong username or password."
        super().__init__(message)


class UserExists(Exception):
    """Raised when user already exists."""

    def __init__(self):
        """Create UserExists exception."""
        message = "User with such name already exists."
        super().__init__(message)


class User:
    """Class User for working with users."""

    def __init__(self, config: dict, username: str = None, password: str = None, token: str = None):
        """Create User.

        Provide config.
        """
        self._conf = config["login"]
        self._conf_db = config["db"]

        self._log = get_colored_logger("User", self._conf["log_level"])

        self.user = None
        self.passw = None
        self.token = None
        self.authenticated = False

        if username and password:
            self.user = username.lower()
            self.passw = hash_pass(password, self._conf["hashsalt"])
            self._auth = "pass"
        elif token:
            self.token = token
            self._auth = "token"
        else:
            raise ValueError("You should provide user/pass pair or token.")

    def _path_auth(self):
        self._log.debug("Authenticating %s with password %s", self.user, self.passw)

        self.token = token_urlsafe(16)

        db = get_db(self._conf_db)
        user = db.users.find_one_and_update(
            {
                "username": self.user,
                "password": self.passw
            },
            {
                "$set": {
                    "last_login": dt.datetime.utcnow(),
                    "token": self.token
                }
            },
            return_document=ReturnDocument.AFTER
        )
        if not user:
            raise AuthFailed()

    def _token_auth(self):
        db = get_db(self._conf_db)
        user = db.users.find_one_and_update(
            {
                "token": self.token
            },
            {
                "$set": {
                    "last_login": dt.datetime.utcnow()
                }
            },
            return_document=ReturnDocument.AFTER
        )
        if not user:
            raise AuthFailed()
        self.user = user["username"]
        self.passw = user["password"]

    def auth(self, username: str = None, password: str = None):
        """Authenticate user.

        If successfull returns list of users.
        """
        if username and password:
            self.user = str(username)
            self.passw = str(hash_pass(password, self._conf["hashsalt"]))

        if self._auth == "pass":
            self._path_auth()
        elif self._auth == "token":
            self._token_auth()
        else:
            raise ValueError("You should provide user/pass pair or token.")

        db = get_db(self._conf_db)
        users = list(
            db.users.find(
                {
                    "username": {"$ne": self.user}
                },
                {"_id": False, "password": False, "token": False}
            )
        )

        response = {
            "token": self.token,
            "users": users
        }
        self.authenticated = True
        return response

    def register(self):
        """Register new user."""
        db = get_db(self._conf_db)
        existing_user = db.users.find_one(
            {
                "username": self.user,
            }
        )
        if existing_user:
            raise UserExists()

        db.users.insert_one(
            {
                "username": self.user,
                "password": self.passw,
                "created_on": dt.datetime.utcnow(),
                "last_login": dt.datetime.utcnow()
            }
        )
        return {"message": "ok"}

    def logout(self):
        """Erase access token."""
        if self._auth == "pass":
            request = {"username": self.user}
        else:
            request = {"token": self.token}

        db = get_db(self._conf_db)
        db.users.update_one(
            request,
            {"$unset": {"token": None}}
        )


def hash_pass(password: str, salt: str = ": ]"):
    """Hashes password."""
    salted_pass = salt + password
    md5hash = md5()
    md5hash.update(salted_pass.encode("utf-8"))
    encoded_pass = md5hash.hexdigest()

    return encoded_pass
