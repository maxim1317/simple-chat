"""Chat room."""
import datetime as dt
from hashlib import md5, sha1
from pymongo import ReturnDocument, DESCENDING

from sibutils.db import get_db
from sibutils.logging import get_colored_logger

from api.user import User


class Room:
    """Chat room."""

    def __init__(
            self,
            config: dict,
            user_from: User = None,
            user_to: str = None,
            room_id: str = None
    ):
        """Create Room."""
        self._conf = config["login"]
        self._conf_db = config["db"]

        self._log = get_colored_logger("User", self._conf["log_level"])

        self.user_from = user_from
        self.user_to = user_to
        self.room_id = None
        self.room: dict = dict()

        if not user_from:
            raise ValueError("You must provide user_from")
        if not (user_to or room_id):
            raise ValueError("You must provide user_to or room_id")

        if user_from and user_to:
            if not user_from.authenticated:
                user_from.auth()

            self.user_from = user_from
            self.user_to = user_to.lower()

            hash_1 = int(sha1(self.user_from.user.encode()).hexdigest(), 16) % (10 ** 8)
            hash_2 = int(sha1(self.user_to.encode()).hexdigest(), 16) % (10 ** 8)
            md5hash = md5()
            md5hash.update(str(hash_1 + hash_2).encode())
            room_id = md5hash.hexdigest()
            self.room_id = md5hash.hexdigest()
        elif room_id:
            self.room_id = room_id

    def fetch_all(self) -> dict:
        db = get_db(self._conf_db)

        set_on_insert: dict = {
            "$set": {
                "last_mesg_on": dt.datetime.utcnow()
            }
        }
        if (self.user_from and self.user_to):
            set_on_insert = {
                "$setOnInsert": {
                    "users": [self.user_from.user, self.user_to],
                    "room_id": self.room_id,
                    "last_mesg_on": dt.datetime.utcnow(),
                    "created_on": dt.datetime.utcnow(),
                    "messages": []
                }
            }
        self.room = db.rooms.find_one_and_update(
            {
                "room_id": self.room_id
            },
            set_on_insert,
            upsert=True,
            return_document=ReturnDocument.AFTER
        )
        messages = list(
            db.messages.find(
                {"_id": {"$in": self.room["messages"]}},
                {"_id": 0}
            ).sort([("created_on", DESCENDING)])
        )

        for i, message in enumerate(messages):
            messages[i]["sent_on"] = dt.datetime.timestamp(message["sent_on"])

        response = {
            "room_id": self.room_id,
            "created_on": dt.datetime.timestamp(self.room["created_on"]),
            "last_mesg_on": dt.datetime.timestamp(self.room["last_mesg_on"]),
            "messages": messages
        }
        return response

    def fetch_since(self, time: dt.datetime):
        if not self.room:
            self.fetch_all()

        db = get_db(self._conf_db)
        messages = list(
            db.messages.find(
                {
                    "room_id": self.room_id,
                    "sent_on": {"$gte": time}
                },
                {"_id": 0}
            ).sort([("sent_on", DESCENDING)])
        )

        for i, message in enumerate(messages):
            messages[i]["sent_on"] = dt.datetime.timestamp(message["sent_on"])

        response = {
            "room_id": self.room_id,
            "created_on": self.room["created_on"],
            "last_mesg_on": self.room["last_mesg_on"],
            "messages": messages
        }
        return response

    def send_message(self, message: str) -> dict:
        db = get_db(self._conf_db)
        msg = db.messages.insert_one(
            {
                "room_id": self.room_id,
                "from": self.user_from.user,
                "sent_on": dt.datetime.utcnow(),
                "message": message
            }
        )
        response = {
            "message": "Message sent."
        }
        self.room = db.rooms.find_one_and_update(
            {
                "room_id": self.room_id
            },
            {
                "$set": {"last_mesg_on": dt.datetime.utcnow()},
                "$push": {"messages": msg.inserted_id}
            }
        )
        return response
