#! /bin/bash

###########################################################
######## Обновляет все __init__.py файлы в проекте ########
###########################################################

YELLOW='\e[93m'
GREEN='\e[92m'
RED='\e[91m'
RESET='\e[0m'

usage()
# Prints usage
{
    if [ "$1" == 1 ]; then
        echo -e "${RED}Usage: `basename $0` [ PROJECT_NAME ]${RESET}"
    else
        echo "Usage: `basename $0` [ PROJECT_NAME ]"
    fi
    exit $1
}

py_lib_check()
# Checks if lib is installed
{
    $1 -h > /dev/null \
    || \
    (echo -e "${RED}No ${1} util!${RESET} \nInstall with:\n\tpip3 install ${1}" && exit 1)
}

# Print help if running with "-h"
if [ "$1" == "-h" ]; then
    usage 0
fi

# Checks if project name passed
[ "$#" -eq 1 ] || usage 1

PROJECT="$1"

py_lib_check mkinit

echo -e "${YELLOW}Updating all __init__.py files in ${PROJECT}${RESET}"

# Walk through all subdirectories
dirs=($(find $PROJECT -type d))
for dir in "${dirs[@]}"; do
    echo -e "\tChecking ./${dir}/"
    mkinit --inplace --noattrs ./$dir > /dev/null
done

echo -e "${GREEN}All __init__.py files updated${RESET}"