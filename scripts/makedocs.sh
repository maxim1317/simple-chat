#! /bin/bash

##########################################################
########## Пытается собрать вики из докстрингов ##########
##########################################################

YELLOW='\e[93m'
GREEN='\e[92m'
RED='\e[91m'
RESET='\e[0m'

py_lib_check()
# Checks if lib is installed
{
    $1 -h > /dev/null \
    || \
    (echo -e "${RED}No ${1} util!${RESET} \nInstall with:\n\tpip3 install ${1}" && exit 1)
}

echo -e "${YELLOW}Building the docs${RESET}"


py_lib_check pdoc

mkdir -p docs/
pdoc --html --output-dir docs --force . && echo -e "${GREEN}Docs building is finished${RESET}" && exit 0

echo -e "${RED}Doc building failed${RESET}"